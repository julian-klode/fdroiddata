Categories:Time
License:GPLv3+
Web Site:
Source Code:https://github.com/beriain/PolarClock
Issue Tracker:https://github.com/beriain/PolarClock/issues

Auto Name:Polar Clock
Summary:A polar clock
Description:
A polar clock, inspired by [http://blog.pixelbreaker.com/polarclock the one made
by pixelmaker]:
.

Repo Type:git
Repo:https://github.com/beriain/PolarClock

Build:1.1,2
    commit=1.1
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.1
Current Version Code:2
